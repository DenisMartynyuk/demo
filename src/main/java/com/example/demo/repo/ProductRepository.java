package com.example.demo.repo;


import com.example.demo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Set<Product> findByCategory(String category);
    Optional<Product> findFirstByCategory(String category);
}
