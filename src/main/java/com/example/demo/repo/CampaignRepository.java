package com.example.demo.repo;

import com.example.demo.model.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

public interface CampaignRepository extends JpaRepository<Campaign, Long> {
    List<Campaign> findByCategoryAndStartDateGreaterThanEqual(String category, Instant startDate);
    List<Campaign> findByStartDateGreaterThanEqual(Instant startDate);
}
