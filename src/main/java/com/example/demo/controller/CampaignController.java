package com.example.demo.controller;


import com.example.demo.model.Campaign;
import com.example.demo.model.Product;
import com.example.demo.repo.CampaignRepository;
import com.example.demo.repo.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;


@RestController
@RequestMapping("/campaign")
public class CampaignController {
    private final Logger logger = LoggerFactory.getLogger(CampaignController.class);

    private CampaignRepository campaignRepository;
    private ProductRepository productRepository;

    @Autowired
    public CampaignController(CampaignRepository repository, ProductRepository productRepository) {
        this.campaignRepository = repository;
        this.productRepository = productRepository;
    }

    @PostConstruct
    public void init() {
        List<Product> products = new ArrayList<>();

        String[] categories = new String[] {"book", "food", "clothe", "toy", "car"};
        Random r = new Random(123);

        for (int i = 0; i < 100000; i++) {
            products.add(new Product("product_" + i, categories[r.nextInt(5)], r.nextInt(100000) / 100.0));
        }

        productRepository.save(products);
        logger.info("products added to db");
    }

    @GetMapping("create")
    public ResponseEntity<?> create(@RequestParam("name") String name,
                                           @RequestParam("startDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
                                           @RequestParam("category") String category,
                                           @RequestParam("bid") Double bid) {
        try {
            Campaign campaign = new Campaign(name, Instant.ofEpochMilli(startDate.getTime()), category, bid);
            return ResponseEntity.ok(campaignRepository.saveAndFlush(campaign));
        } catch (Exception x) {
            logger.error("Was not able to create campaign", x);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(x.getMessage()));
        }
    }

    @GetMapping("serve/{category}")
    public ResponseEntity<?> serve(@PathVariable("category") String category) {
        try {
            Set<Product> products = productRepository.findByCategory(category);

            if (products.isEmpty()) return ResponseEntity.ok(findHighestBid());

            Map<Product, Double> productMap = products.stream()
                    .collect(toMap(p -> p, p -> getBidForCategory(p.getCategory())));

            Optional<Product> result = productMap.entrySet().stream()
                    .sorted((a, b) -> b.getValue().compareTo(a.getValue())).findFirst()
                    .map(Map.Entry::getKey);

            if (result.isPresent()) return ResponseEntity.ok(result.get());
        } catch (Exception x) {
            logger.error("Was not able to serve category {}", category, x);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error("Unable to found product for " + category));
    }

    private Product findHighestBid() {
        List<String> sortedCategories = campaignRepository.findByStartDateGreaterThanEqual(Instant.now().minus(Duration.ofDays(10)))
                .stream().collect(groupingBy(Campaign::getCategory))
                .entrySet().stream().collect(toMap(Map.Entry::getKey, e -> e.getValue().stream().map(Campaign::getBid).reduce(Double::sum).orElse(0.0)))
                .entrySet().stream().sorted((a, b) ->b.getValue().compareTo(a.getValue())).map(Map.Entry::getKey).collect(toList());

        for(String category: sortedCategories) {
            Optional<Product> product = productRepository.findFirstByCategory(category);
            if (product.isPresent()) return product.get();
        }

        throw new IllegalStateException("products not found");
    }

    private Double getBidForCategory(String category) {
        return campaignRepository.findByCategoryAndStartDateGreaterThanEqual(category, Instant.now().minus(Duration.ofDays(10)))
                .stream().map(Campaign::getBid).reduce(Double::sum).orElse(0.0);
    }

}
