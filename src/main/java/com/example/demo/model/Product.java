package com.example.demo.model;


import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static javax.persistence.FetchType.EAGER;

@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, columnDefinition = "varchar(40)")
    @Type(type = "uuid-char")
    private UUID serialNumber;
    private String name;
    private String category;
    private Double price;

    public Product() {
    }

    public Product(String name, String category, Double price) {
        this.serialNumber = UUID.randomUUID();
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(UUID serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(serialNumber, product.serialNumber) &&
                Objects.equals(name, product.name) &&
                Objects.equals(category, product.category) &&
                Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, serialNumber, name, category, price);
    }
}
